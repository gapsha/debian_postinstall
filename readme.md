**Подготовка к использованию Ansible**
```
#!bash
aptitude install vim sudo

adduser gapsha sudo

update-alternatives --config editor
```



**SSH**
```
#!bash
ssh-keygen

ssh-agent bash

ssh-add ~/.ssh/id_rsa

ssh-copy-id user@host

ssh user@host
```